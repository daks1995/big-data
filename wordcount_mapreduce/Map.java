public class Map extends Mapper<LongWritable, Text, Text, IntWritable>{
    static enum Counters { INPUT_WORDS }
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private boolean caseSensitive = true;
    private Set<String> patternsToSkip = new HashSet<String>();
    private long numRecords = 0;
    private String inputFile;
    
    private void parseSkipFile(Path patternsFile) {
        try {
            BufferedReader fis = new BufferedReader(new FileReader(patternsFile.toString()));
            String pattern = null;
            while ((pattern = fis.readLine()) != null)
                patternsToSkip.add(pattern);
            fis.close();
        } catch (IOException ioe) {
            System.err.println("Caught exception while parsing the cached file ’" +patternsFile + "’ : " + StringUtils.stringifyException(ioe));
        }
    }
 
    public void setup(Context context) {
        Configuration conf = context.getConfiguration();
        caseSensitive = conf.getBoolean("wordcount.case.sensitive", true);
        inputFile = conf.get("map.input.file");
        
        if (conf.getBoolean("wordcount.skip.patterns", false)) {
            Path[] patternsFiles = new Path[0];
            try {
                patternsFiles = DistributedCache.getLocalCacheFiles(conf);
            } catch (IOException ioe) {
                System.err.println("Caught exception while getting cached files: " +StringUtils.stringifyException(ioe));
            }
            for (Path patternsFile : patternsFiles)
                parseSkipFile(patternsFile);
        }
    }
    
    public void cleanup(Context context) {
        patternsToSkip.clear();
    }

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = caseSensitive ? value.toString() : value.toString().toLowerCase();
        
        for (String pattern : patternsToSkip)
            line = line.replaceAll(pattern, "");
        
        StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            word.set(tokenizer.nextToken());
            context.write(word, one);
            context.getCounter(Counters.INPUT_WORDS).increment(1);
        }

        if ((++numRecords % 100) == 0)
            context.setStatus("Finished processing " + numRecords + " records " +"from the input file: " + inputFile);
    }
}