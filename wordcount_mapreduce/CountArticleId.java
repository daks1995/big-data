package mapreduce;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.apache.hadoop.util.StringUtils;

/**
 *	Program maps article_id and its number of occurences 
 */

public class CountArticleId extends Mapper<LongWritable, Text, Text, IntWritable>{
    static enum Counters { INPUT_WORDS }
    private final static IntWritable one = new IntWritable(1);
    private Text word                    = new Text();
    private boolean caseSensitive        = true;
    private Set<String> patternsToSkip   = new HashSet<String>();
    private long numRecords              = 0;
    private String inputFile;
    
    private void parseSkipFile(String fileName) {
        try {
        	BufferedReader fis = new BufferedReader(new FileReader(fileName));
        	String pattern = null;
	          while ((pattern = fis.readLine()) != null) {
	            patternsToSkip.add(pattern);
	          }
	          fis.close();
        } catch (IOException ioe) {
          System.err.println("Caught exception while parsing the cached file '"
              + StringUtils.stringifyException(ioe));
        }
    }
 
	public void setup(Context context) {
        Configuration conf = context.getConfiguration();
        caseSensitive      = conf.getBoolean("wordcount.case.sensitive", true);
        inputFile          = conf.get("map.input.file");
        
        if (conf.getBoolean("wordcount.skip.patterns", false)) {
            try {
                URI[] patternsURIs = context.getCacheFiles();
                for (URI patternsURI : patternsURIs) {
                  Path patternsPath = new Path(patternsURI.getPath());
                  String patternsFileName = patternsPath.getName().toString();
                  parseSkipFile(patternsFileName);
                }
            } catch (IOException ioe) {
                System.err.println("Caught exception while getting cached files: " +StringUtils.stringifyException(ioe));
            }
        }
    }
    
    public void cleanup(Context context) {
        patternsToSkip.clear();
    }

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = caseSensitive ? value.toString() : value.toString().toLowerCase();
        
        for (String pattern : patternsToSkip)
            line = line.replaceAll(pattern, "");
        
        StringTokenizer tokenizer = new StringTokenizer(line);

        while (tokenizer.hasMoreTokens()) {
        	String tk = tokenizer.nextToken();
            if(tk.equals("REVISION")) {
            	/* Move to next record to fetch article_id if
            	 * the current token is REVISION and only insert
            	 * that number into context 
            	 * */
            	word.set(tokenizer.nextToken());
                context.write(word, one);
                context.getCounter(Counters.INPUT_WORDS).increment(1);
            }
        }

        if ((++numRecords % 100) == 0)
            context.setStatus("Finished processing " + numRecords + " records " +"from the input file: " + inputFile);
    }
}