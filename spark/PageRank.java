package spark;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;

import com.google.common.collect.Iterables;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import scala.Tuple2;

public class PageRank {
	private static Date endTime    	= null;
	private static DateFormat df	= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private static String pageName	= "";
	private static boolean addLink  = false;
	private static String  DELIM    = " ";
	private static int N_ITER       = 2;
	private static int N_ROWS       = 14;
	private static Double D_FACTOR  = 0.85;
	
	private static class addValues implements Function2<Double, Double, Double> {
			@Override
		    public Double call(Double x1, Double x2) {
		      return x1 + x2;
		    }
	}

	/**
	 *  Sort based on double input key value
	 * @param ranks
	 * @param sortBy
	 * @return
	 */
    static JavaPairRDD<Double,String> sortByValue(JavaPairRDD<Double,String> ranks, String sortBy){
            if( sortBy.equals("asc") )          return ranks.sortByKey(true); 
            else if( sortBy.equals("desc") )    return ranks.sortByKey(false);
            else return ranks;
    }

	public static void main(String[] args) {
		/**
		 *  Check if number of arguments is less than 4 if yes then
		 *  inform user and exit from program
		 */
		if(args.length<4) {
			System.out.println("Oops! Incorrect number of arguments passed:");
			System.out.println("This program takes in 4 arguments, <inputpath> <outputpath> <iterations> <data in ISO format>");
			System.exit(1);
		};
		
		/**
		 *  Check and parse number of iterations
		 */
		if(args.length>2) N_ITER=Integer.parseInt(args[2]);
		
		/**
		 *  Check and parse end date value
		 */
		if(args.length>3) {
			try {
				endTime = df.parse(args[3]);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}

		/**
		 * Create Spark Context
		 */
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("PageRank"));
		
		/**
		 * Create Spark Context
		 */
		JavaPairRDD<String,String> wikiData = sc.wholeTextFiles(args[0]);
		
		/**
		 * Parse entire data based on end date and required format
		 */
		JavaRDD<String> wikiLines = wikiData.flatMap(s ->{

			String[] lines = s._2().split("\n");

			ArrayList<String> links = new ArrayList<String>();
			String allOutNodes 		= null;
			
			for(int i=0;i<lines.length;i++) {
				StringTokenizer tokenizer = new StringTokenizer(lines[i]);
				String[] nxtRecord        = null;
				if((i+N_ROWS)<lines.length)	nxtRecord = lines[i+N_ROWS].split(DELIM);
				
				while( tokenizer.hasMoreTokens() ) {
					 String tk = tokenizer.nextToken();
			            /**
			             * Check if this is new article
			             */
			         if(tk.equals("REVISION")) {
			                addLink = false;
			                tokenizer.nextToken();
			                tokenizer.nextToken();
			                pageName= tokenizer.nextToken();
			                try {
			                    /**
			                     * Read article_revision time and check if it predates entered time. if yes then set addLink to true
			                     */
			                    if( df.parse(tokenizer.nextToken()).before(endTime) ) {
			                    	if( nxtRecord!=null  && pageName.equals(nxtRecord[3]) && df.parse(nxtRecord[4]).before(endTime) ) {
			                    		addLink = false;
			                    	}else {
			                    		addLink = true;
			                    	}
			                    }
			                } catch (ParseException e) {
			                    e.printStackTrace();
			                }
			          }
			         if(tk.equals("MAIN") && addLink)  {
			                /**
			                 * If current line being processed starts with MAIN and if its
			                 * revision time predates entered time then create graph edge
			                 */
			            	allOutNodes = pageName+DELIM;

			            	links.add(pageName+DELIM+pageName);

		                    while(tokenizer.hasMoreTokens()) {
		                        /**
		                         *  Check and add an edge only if it doesn't exists.
		                         *  If already an edge exists then ignore such cases
		                         */
		                        String outLink = tokenizer.nextToken();
		                        /**
		                         * Ignore self-loops
		                         */
		                        if( pageName.equals(outLink) ) continue;
		                        
    							if( !allOutNodes.contains(DELIM+outLink+DELIM) ) {
    								allOutNodes+=(outLink+DELIM);
    								links.add(pageName+DELIM+outLink);
    							}
		                    }
			          }
				}
			}
			return links;
		});
		
	    /** Create article_title <link1>
	     *         article_title <link2>
	     *         ...           ...
	     *         article_title <linkn>
	     */
	    JavaPairRDD<String, Iterable<String>> links = wikiLines.mapToPair(new PairFunction<String, String, String>() {
	    	@Override
	      public Tuple2<String, String> call(String s) {
	        String[] parts = s.split(DELIM);
	        if(parts[0].equals(parts[1])) {
	        	return new Tuple2<String, String>(parts[0], parts[0]+DELIM+parts[1]);
	        }else {
	        	return new Tuple2<String, String>(parts[0], parts[1]);
	        }
	      }
	    }).groupByKey().cache();

	    /**
	     *  Initialize rank of 1.0 for each outLink
	     */
	    JavaPairRDD<String, Double> pageRanks = links.mapValues(new Function<Iterable<String>, Double>() {
			@Override
	        public Double call(Iterable<String> rs) {
	          return 1.0;
	        }
	     });

	    /**
	     *  Execute PageRank algorithm for N_ITER number of iterations.
	     */
	    for (int i = 0; i<N_ITER; i++) {
	    	/**
	    	 * Calculate contributions other links to current one.
	    	 */
	        JavaPairRDD<String, Double> otherRanks = links.join(pageRanks).values()
	          .flatMapToPair(s -> {
	            int urlCount = Iterables.size(s._1());
	            List<Tuple2<String, Double>> results = new ArrayList<>();
	            String [] nodes;
	            for (String n : s._1) {
	            	nodes = n.split(DELIM);
	            	if(urlCount==1) {
	            		results.add(new Tuple2<>(nodes[0], 0.0));
	            	}else if(nodes.length<2){
	            		results.add(new Tuple2<>(n, s._2() / (urlCount-1)));
	            	}else if(nodes.length==2){
	            		results.add(new Tuple2<>(nodes[0], 0.0));
	            	}
	            }
	            return results;
	        });
	        /**
	         * Re-calculate PageRank based on other connected links
	         */
	        pageRanks = otherRanks.reduceByKey(new addValues()).mapValues(sum -> (1-D_FACTOR) + sum * D_FACTOR);
	    }

	    /**
	     * Convert from Tuple2<String,Double> to Tuple2<Double,String>
	     */
	    JavaPairRDD<Double,String> frequencies = pageRanks.mapToPair( new PairFunction<Tuple2<String, Double>, Double, String>() { 
	    	@Override 
	        public Tuple2<Double,String> call(Tuple2<String, Double> s) {
	    		return new Tuple2<Double,String>(s._2, s._1); 
	        }
	    });

	    try {
			JavaPairRDD<Double,String> sortedByFreq = sortByValue(frequencies, "desc");
		    /**
		     * Convert from Tuple2<Double,String> to Tuple2<String,Double>
		     */
			pageRanks = sortedByFreq.mapToPair( new PairFunction<Tuple2<Double,String>,String,Double>() {
				@Override 
				public Tuple2<String,Double> call(Tuple2<Double, String> s) {
					return new Tuple2<String,Double>(s._2, s._1);
				} 
		    });
		    /**
		     * Convert output into required format <article_id> <pagerank>
		     */
			JavaRDD<String>output = pageRanks.map(s->s._1+DELIM+s._2);
			/**
			 * Save into a output file
			 */
			output.saveAsTextFile(args[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		sc.close();
	}
}